
	// 1. Create a function which is able to prompt the user to provide their fullname, age, and location.
	// 	-use prompt() and store the returned value into a function scoped variable within the function.
	// 	-display the user's inputs in messages in the console.
	// 	-invoke the function to display your information in the console.
	// 	-follow the naming conventions for functions.

	
	//first function here:
	function userInfo(){
		let fullName = prompt('Enter your full name:')
		let age = prompt('Enter your age:')
		let location = prompt ('Enter your address:')

		console.log('Hi ' + fullName );
		console.log(fullName + "'s age is " + age);
		console.log(fullName + " is located at " + location);
	};
	userInfo();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function faveArtists(){
		let artists = ['Eminem', 'Maroon 6', 'Linkin Park', 'Nirvana', 'Post Malone'];
		console.log(artists);
	};
    faveArtists();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function faveMovies(){
		let movie1 = "The Godfather";
		let movie2 = "The Godfather Part II";
		let movie3 = "Catch Me If You Can";
		let movie4 = "Zodiac";
		let movie5 = "Shutter Island";

		console.log("1. " + movie1);
		console.log("Tomatometer for " + movie1 + ": 97%");
		console.log("2. " + movie2);
		console.log("Tomatometer for " + movie2 + ": 96%");
		console.log("3. " + movie3);
		console.log("Tomatometer for " + movie3 + ": 96%");
		console.log("4. " + movie4);
		console.log("Tomatometer for " + movie4 + ": 89%");
		console.log("5. " + movie5);
		console.log("Tomatometer for " + movie5 + ": 68%");
	};

	faveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();
// console.log(friend1);
// console.log(friend2);